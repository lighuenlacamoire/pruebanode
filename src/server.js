var express = require('express');
var bodyParser = require('body-parser');
var app = express();
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');

app.use(bodyParser.json());
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.get('/api/Hello', async (req, res) => {

  res.send(JSON.stringify({ name: 'marquitos'}));
});

app.listen(8000, function() {
  console.log('Servidor rodando na porta 8000.');
});
